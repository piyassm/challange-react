export const Url = {
  BASE_URL_API: "https://my-json-server.typicode.com/mamaiank/challange-react",
};
export const Request = {
  API_CHARITIES: Url.BASE_URL_API + "/charities",
  API_PAYMENTS: Url.BASE_URL_API + "/payments",
};
